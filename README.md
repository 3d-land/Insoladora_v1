# Insoladora_v1
Insulator with time control using electronic parts such as 5mm UV LEDs and PIC microcontroller.

# Guide (Spanish)
======
[Blog](https://cc-electrognu.blogspot.com/)

# Images
======

# PCB (Kicad)
======

# 3D Model (Freecad)
======

License
======

The insolator  is licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/)

Attributions
============
- @fandres323
- @maurinc2010